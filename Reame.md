## Môi trường

- Sửa sdk in file C:\Program Files\Java\jdk-11.0.12\conf\security\java.security find jdk.certpath.disabledAlgorithms and
  delete TLSv1
- Cài sqlserve 2012 (https://www.microsoft.com/en-us/download/details.aspx?id=56040), import file db dump (link down
  file: https://drive.google.com/file/d/10gn2wOkMepxFDjPaqsXg5DqbYrPEqj_u/view?usp=sharing), add user và sửa thông tin
  vào file application.properties

## Hướng dẫn

- Làm tương tự class User chỉ sửa tên bảng và câu query
- Khai báo tên class vào Runner đúng theo thứ tự quan hệ trong DB

TRUNCATE TABLE products.product_report_log CASCADE; TRUNCATE TABLE users.user_subscriber CASCADE; TRUNCATE TABLE
messages.message_type CASCADE; TRUNCATE TABLE orders.order_item CASCADE; TRUNCATE TABLE payments.banks CASCADE; TRUNCATE
TABLE payments.partners CASCADE; TRUNCATE TABLE payments.payment_method CASCADE; TRUNCATE TABLE
information_schema.sql_implementation_info CASCADE; TRUNCATE TABLE information_schema.sql_parts CASCADE; TRUNCATE TABLE
information_schema.sql_sizing CASCADE; TRUNCATE TABLE information_schema.sql_features CASCADE; TRUNCATE TABLE
products.comment CASCADE; TRUNCATE TABLE products.rel_payment_method CASCADE; TRUNCATE TABLE transports.partner_store
CASCADE; TRUNCATE TABLE users.user_log CASCADE; TRUNCATE TABLE wallets.transaction CASCADE; TRUNCATE TABLE
wallets.wallet CASCADE; TRUNCATE TABLE public.payment_method CASCADE; TRUNCATE TABLE public.currencyunit CASCADE;
TRUNCATE TABLE public.country CASCADE; TRUNCATE TABLE public.province CASCADE; TRUNCATE TABLE products.product_log
CASCADE; TRUNCATE TABLE orders.order_log CASCADE; TRUNCATE TABLE public.district CASCADE; TRUNCATE TABLE public.ward
CASCADE; TRUNCATE TABLE users.user_report_log CASCADE; TRUNCATE TABLE public.bank CASCADE; TRUNCATE TABLE
orders.order_stats CASCADE; TRUNCATE TABLE orders.transport_tracking CASCADE; TRUNCATE TABLE orders.timeline CASCADE;
TRUNCATE TABLE posts.posts_type CASCADE; TRUNCATE TABLE transports.partner CASCADE; TRUNCATE TABLE public.member_type
CASCADE; TRUNCATE TABLE public.agency_type CASCADE; TRUNCATE TABLE public.business_type CASCADE; TRUNCATE TABLE
public.enterprise_type CASCADE; TRUNCATE TABLE public.order_status CASCADE; TRUNCATE TABLE stores.store_report_log
CASCADE; TRUNCATE TABLE products.product_stats CASCADE; TRUNCATE TABLE users.store_follow CASCADE; TRUNCATE TABLE
blogs.author CASCADE; TRUNCATE TABLE blogs.blog_seo CASCADE; TRUNCATE TABLE public.system_email CASCADE; TRUNCATE TABLE
public.config_system CASCADE; TRUNCATE TABLE public.notification_content CASCADE; TRUNCATE TABLE public.email_content
CASCADE; TRUNCATE TABLE public.banner CASCADE; TRUNCATE TABLE payments.payments CASCADE; TRUNCATE TABLE
orders.order_store CASCADE; TRUNCATE TABLE ratings.store_point_config CASCADE; TRUNCATE TABLE public.static_data
CASCADE; TRUNCATE TABLE orders.orders CASCADE; TRUNCATE TABLE products.category_group CASCADE; TRUNCATE TABLE
users.user_company CASCADE; TRUNCATE TABLE users.user_contact CASCADE; TRUNCATE TABLE users.customer_note CASCADE;
TRUNCATE TABLE stores.store_package CASCADE; TRUNCATE TABLE users.users_role CASCADE; TRUNCATE TABLE users.user CASCADE;
TRUNCATE TABLE messages.message CASCADE; TRUNCATE TABLE teams.team CASCADE; TRUNCATE TABLE users.token CASCADE; TRUNCATE
TABLE users.trackingstore CASCADE; TRUNCATE TABLE users.user_address CASCADE; TRUNCATE TABLE users.user_token CASCADE;
TRUNCATE TABLE users.bank_account CASCADE; TRUNCATE TABLE posts.post CASCADE; TRUNCATE TABLE users.business_booth
CASCADE; TRUNCATE TABLE users.individual_booth CASCADE; TRUNCATE TABLE quotations.rfq CASCADE; TRUNCATE TABLE
quotations.quotation_rfq CASCADE; TRUNCATE TABLE posts.post_bid_invitation CASCADE; TRUNCATE TABLE
posts.post_bidinvitationtownhouse CASCADE; TRUNCATE TABLE posts.post_business_product CASCADE; TRUNCATE TABLE
posts.post_buy_information CASCADE; TRUNCATE TABLE posts.post_register_team CASCADE; TRUNCATE TABLE posts.post_team
CASCADE; TRUNCATE TABLE quotations.quotation_product CASCADE; TRUNCATE TABLE products.category CASCADE; TRUNCATE TABLE
products.attribute CASCADE; TRUNCATE TABLE products.attribute_value CASCADE; TRUNCATE TABLE
products.rel_product_attribute CASCADE; TRUNCATE TABLE stores.store CASCADE; TRUNCATE TABLE stores.store_address
CASCADE; TRUNCATE TABLE stores.store_business CASCADE; TRUNCATE TABLE stores.store_document CASCADE; TRUNCATE TABLE
stores.store_file CASCADE; TRUNCATE TABLE stores.store_policy CASCADE; TRUNCATE TABLE users.cart CASCADE; TRUNCATE TABLE
stores.store_letter CASCADE; TRUNCATE TABLE products.category_store CASCADE; TRUNCATE TABLE stores.store_follower
CASCADE; TRUNCATE TABLE stores.store_capacity CASCADE; TRUNCATE TABLE stores.store_personal_info CASCADE; TRUNCATE TABLE
stores.store_cooperative_factory CASCADE; TRUNCATE TABLE stores.store_equipment CASCADE; TRUNCATE TABLE
stores.store_process CASCADE; TRUNCATE TABLE stores.store_product_line CASCADE; TRUNCATE TABLE stores.store_contact
CASCADE; TRUNCATE TABLE stores.store_profile CASCADE; TRUNCATE TABLE public.unit CASCADE; TRUNCATE TABLE public.currency
CASCADE; TRUNCATE TABLE blogs.category CASCADE; TRUNCATE TABLE products.product CASCADE; TRUNCATE TABLE
products.product_favorite CASCADE; TRUNCATE TABLE products.product_reference CASCADE; TRUNCATE TABLE blogs.blog CASCADE;
TRUNCATE TABLE products.product_wholesale CASCADE; TRUNCATE TABLE products.rating CASCADE; TRUNCATE TABLE
products.product_variant CASCADE; TRUNCATE TABLE users.cart_item CASCADE; TRUNCATE TABLE users.cart_group CASCADE;
TRUNCATE TABLE products.variant_group CASCADE; TRUNCATE TABLE users.cart_group_member CASCADE; TRUNCATE TABLE
products.inventory CASCADE; TRUNCATE TABLE products.product_image CASCADE;
