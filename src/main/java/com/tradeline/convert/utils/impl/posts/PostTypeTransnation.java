package com.tradeline.convert.utils.impl.posts;

import com.tradeline.convert.utils.BaseUntil;

import java.util.List;

public class PostTypeTransnation extends BaseUntil {
  @Override
  public String getSourceSelectQuery() {
    return "select suptt.ID as id, suptt.LanguageCode as language_code, suptt.ID_Type as post_type_id,suptt.TypeName as name from Styles_Users_Post_Types_Translation suptt ";
  }

  @Override
  public String getDestTable() {
    return "posts.post_type_transnation";
  }

  @Override
  public List<String> getEnumColumns() {
    return null;
  }
}
