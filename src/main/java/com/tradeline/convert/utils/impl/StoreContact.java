package com.tradeline.convert.utils.impl;

import com.tradeline.convert.utils.BaseUntil;

import java.util.Arrays;
import java.util.List;

public class StoreContact extends BaseUntil {
    @Override
    public String getSourceSelectQuery() {
        return "select su.ID_User as ID, su.Email as email, su.CellPhone as phone, su.RegTime AS created_at, su.UpgradeTime AS updated_at \n" +
                "from Styles_Users su inner join Styles_Users_Translation sut on su.ID_User = sut.ID_User  \n" +
                "where su.IsStore =1  and sut.LanguageCode = 'vi'";
    }

    @Override
    public String getDestTable() {
        return "stores.store_contact";
    }

    @Override
    public List<String> getEnumColumns() {
        return null;
    }
}
