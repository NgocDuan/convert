package com.tradeline.convert.utils.impl;

import com.tradeline.convert.utils.BaseUntil;

import java.util.List;

public class Currency extends BaseUntil {
    @Override
    public String getSourceSelectQuery() {
        return "select ID_Currency as id, CurrencyCode as code, Symbol as name, 'd' as short_code from dbo.Styles_Shops_Currencies";
    }

    @Override
    public String getDestTable() {
        return "public.currency";
    }

    @Override
    public List<String> getEnumColumns() {
        return null;
    }
}
