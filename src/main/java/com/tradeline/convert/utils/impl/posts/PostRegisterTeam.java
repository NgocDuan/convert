package com.tradeline.convert.utils.impl.posts;

import com.tradeline.convert.utils.BaseUntil;

import java.util.List;

public class PostRegisterTeam extends BaseUntil {
    @Override
    public String getSourceSelectQuery() {
        return "select p.ID_Post_Team as id_post, p.FullName as major, t.Description as description, 1 as experience, 1 as person_number, 'hour' as time_unit, t.Title as title, 1 as province_id  from Styles_Users_Posts_Team as p inner join Styles_Users_Posts_Team_Translation as t on p.ID_Post_Team = t.ID_Post_Team and t.LanguageCode = 'vi'";
    }

    @Override
    public String getDestTable() {
        return "posts.post_register_team";
    }

    @Override
    public List<String> getEnumColumns() {
        return null;
    }
}
