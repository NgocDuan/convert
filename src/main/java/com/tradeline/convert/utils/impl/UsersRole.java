package com.tradeline.convert.utils.impl;

import com.tradeline.convert.utils.BaseUntil;

import java.util.List;

public class UsersRole extends BaseUntil {
    @Override
    public String getSourceSelectQuery() {
        return "select ID as id, RoleName as rolename, Hidden as hidden\n" +
                "from Styles_Users_Roles_Translation where LanguageCode='vi'";
    }

    @Override
    public String getDestTable() {
        return "users.users_role";
    }

    @Override
    public List<String> getEnumColumns() {
        return null;
    }
}
