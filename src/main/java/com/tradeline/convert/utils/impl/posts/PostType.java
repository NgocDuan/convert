package com.tradeline.convert.utils.impl.posts;

import com.tradeline.convert.utils.BaseUntil;

import java.util.List;

public class PostType extends BaseUntil {
  @Override
  public String getSourceSelectQuery() {
    return "select p.ID_Type as id , pt.TypeName as name, (CASE WHEN p.Active = 1 THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END) AS active from Styles_Users_Post_Types p left join Styles_Users_Post_Types_Translation pt on p.ID_Type = pt.ID_Type and pt.LanguageCode = 'vi'";
  }

  @Override
  public String getDestTable() {
    return "posts.posts_type";
  }

  @Override
  public List<String> getEnumColumns() {
    return null;
  }
}
