package com.tradeline.convert.utils.impl;

import com.tradeline.convert.utils.BaseUntil;

import java.util.List;

public class ProductVariant extends BaseUntil {

    @Override
    public String getSourceSelectQuery() {
        return "select spt.ID_Product as id,  spt.ID_Product as product_id, sp.ProductPrice as price,case when sp.ProductDiscounts > 0 then sp.ProductDiscounts else sp.ProductPrice end as sale_price, sp.AddTime as created_at, sp.EditTime as updated_at\n" +
                "from dbo.Styles_Shops_Products sp inner join dbo.Styles_Shops_Products_Translation spt\n" +
                "on sp.ID_Product = spt.ID_Product where spt.LanguageCode = 'vi'";
    }

    @Override
    public String getDestTable() {
        return "products.product_variant";
    }

    @Override
    public List<String> getEnumColumns() {
        return null;
    }
}
