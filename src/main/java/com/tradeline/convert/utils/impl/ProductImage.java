package com.tradeline.convert.utils.impl;

import com.tradeline.convert.utils.BaseUntil;

import java.util.Arrays;
import java.util.List;

public class ProductImage extends BaseUntil {

  @Override
  public String getSourceSelectQuery() {
//    return "select spt.ID_Product as product_id, Image as image, AddTime as created_at, EditTime as updated_at from dbo.Styles_Shops_Products sp inner join dbo.Styles_Shops_Products_Translation spt\n" +
//            "on sp.ID_Product = spt.ID_Product where spt.LanguageCode = 'vi'";
    return "select sp.ID_Product as product_id, spi.Image as image, AddTime as created_at, EditTime as updated_at\n" +
            " from Styles_Shops_Products_Images spi inner join Styles_Shops_Products sp on sp.ID_Product = spi.ID_Product";
  }

  @Override
  public String getDestTable() {
    return "products.product_image";
  }

  @Override
  public List<String> getEnumColumns() {
    return null;
  }

  @Override
  public List<String> afterConvertQuery() {
    return Arrays.asList(
        "update products.product_image set image=concat('/images/2022-04-11/',split_part(image,'/',array_upper(string_to_array(image,'/'), 1))) where image is not null and image <> ''");
  }
}
