package com.tradeline.convert.utils.impl.posts;

import com.tradeline.convert.utils.BaseUntil;

import java.util.Arrays;
import java.util.List;

public class Post extends BaseUntil {
  @Override
  public String getSourceSelectQuery() {
    // return "SELECT p.ID_Post as id, p.ID_Type as id_posttype, p.ID_User as id_user, (CASE WHEN
    // p.Active = 1 THEN 'APPROVED' ELSE 'REJECT' END) as status, p.TotalView as view_count,
    // p.AddTime as created_at,p.EditTime as updated_at FROM styles_tmdtnew.dbo.Styles_Users_Posts p
    // inner join Styles_Users su on su.ID_User = p.ID_User ";
    return "select ID_Post_Team as id, AddTime as created_at, EditTime as updated_at, 5 as id_posttype, ID_User as id_user, case when Active = 1 then 'APPROVED' else 'REJECT' end as status, 0 as view_count from Styles_Users_Posts_Team\n" +
            "UNION\n" +
            "select sua.ID_Agency as id, sua.AddTime as created_at, EditTime as updated_at, 3 as id_posttype, sua.ID_User as id_user, (CASE WHEN sua.Active = 1 THEN 'APPROVED' ELSE 'REJECT' END) as status, 0 as view_count from Styles_Users_Agencys sua inner join Styles_Users_Agencys_Translation suat on sua.ID_Agency = suat.ID_Agency where sua.IsAgency = 1\n" +
            "UNION\n" +
            "select sua.ID_Agency as id, sua.AddTime as created_at, EditTime as updated_at, 2 as id_posttype, sua.ID_User as id_user, (CASE WHEN sua.Active = 1 THEN 'APPROVED' ELSE 'REJECT' END) as status, 0 as view_count from Styles_Users_Agencys sua inner join Styles_Users_Agencys_Translation suat on sua.ID_Agency = suat.ID_Agency where sua.IsAgency = 0";
  }

  @Override
  public String getDestTable() {
    return "posts.post";
  }

  @Override
  public List<String> getEnumColumns() {
    return Arrays.asList("status");
  }
}
