package com.tradeline.convert.utils.impl;

import com.tradeline.convert.utils.BaseUntil;

import java.util.Arrays;
import java.util.List;

public class RelProductAttribute extends BaseUntil {

    @Override
    public String getSourceSelectQuery() {
        return "select spfvt.ID_Variant as attribute_value_id, spfv.ID_Product as product_id\n" +
                "from Styles_Shops_Products_Feature_Variants_Translation spfvt inner join Styles_Shops_Products_Features_Values spfv\n" +
                "on spfvt.ID_Variant = spfv.ID_Variant where spfvt.LanguageCode = 'vi'";

    }

    @Override
    public String getDestTable() {
        return "products.rel_product_attribute";
    }

    @Override
    public List<String> getEnumColumns() {
        return null;
    }
}
