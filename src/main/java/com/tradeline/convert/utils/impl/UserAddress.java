package com.tradeline.convert.utils.impl;

import com.tradeline.convert.utils.BaseUntil;

import java.util.List;

public class UserAddress extends BaseUntil {
  //  Câu query select dữ liệu bên DB cũ
  @Override
  public String getSourceSelectQuery() {
    return "select ID_User as user_id, CAST(Address as nvarchar(50)) as address,CAST(Telephone as nvarchar(12)) as phone, 1 as district_id, 1 as province_id, 1 as country_id, 6247 as ward_id\n" +
            "from Styles_Users";
  }

  //  Bảng dích bên DB mới
  @Override
  public String getDestTable() {
    return "users.user_address";
  }

  @Override
  public List<String> getEnumColumns() {
    return null;
  }
}
