package com.tradeline.convert.utils.impl;

import com.tradeline.convert.utils.BaseUntil;

import java.util.Arrays;
import java.util.List;

public class StorePackage extends BaseUntil {
    @Override
    public String getSourceSelectQuery() {
        return "select ID_Role as Id, RoleName as name from Styles_Users_Roles_Translation where LanguageCode = 'vi'";
    }

    @Override
    public String getDestTable() {
        return "stores.store_package";
    }

    @Override
    public List<String> getEnumColumns() {
        return null;
    }
}
