package com.tradeline.convert.utils.impl;

import com.tradeline.convert.utils.BaseUntil;

import java.util.Arrays;
import java.util.List;

public class Store extends BaseUntil {
  @Override
  public String getSourceSelectQuery() {
    return "select uimg.Images as images,su.ID_User as ID, su.CompanyName as name,sut.Slogan as slogan, case when su.Active = 1 then 'ACTIVE' else 'INACTIVE' end as state, \n" +
            "su.Image as image, su.ID_Role as package_id,su.Logo as avatar\n" +
            "from Styles_Users su inner join Styles_Users_Translation sut on su.ID_User = sut.ID_User \n" +
            "left join (select ID_User,Images = + '{' + STUFF((select  ','  + '\"'  +Concat('/images/2022-04-11', REVERSE(SUBSTRING(REVERSE(Image), 1, \n" +
            "    CHARINDEX('/', REVERSE(Image), 1) )))   + '\"'   \n" +
            "from Styles_Users_Images ui where ui.ID_User = u.ID_User FOR XML PATH(''), TYPE ).value('.', 'NVARCHAR(MAX)'), 1, 1, ''  ) + '}' \n" +
            "from Styles_Users u Group by u.ID_User) as uimg on su.ID_User = uimg.ID_User\n" +
            "where su.IsStore =1 and sut.LanguageCode = 'vi'\n";
  }

  @Override
  public String getDestTable() {
    return "stores.store";
  }

  @Override
  public List<String> getEnumColumns() {
    return Arrays.asList("state");
  }

  @Override
  public List<String> getJsonColumns() {
    return Arrays.asList("images");
  }

  @Override
  public List<String> afterConvertQuery() {
    return Arrays.asList(
        "update stores.store set image=concat('/images/2022-04-11/',split_part(image,'/',array_upper(string_to_array(image,'/'), 1))) where image is not null and image <> ''",
            "update stores.store set avatar=concat('/images/2022-04-11/',split_part(avatar,'/',array_upper(string_to_array(avatar,'/'), 1))) where avatar is not null and avatar <> ''"
            );
  }
}
