package com.tradeline.convert.utils.impl;

import com.tradeline.convert.utils.BaseUntil;

import java.util.Arrays;
import java.util.List;

public class User extends BaseUntil {
  //  Câu query select dữ liệu bên DB cũ
  @Override
  public String getSourceSelectQuery() {
    return "SELECT ID_user AS id,"
        + "       FullName AS fullname,"
        + "       Telephone AS phone,"
        + "       Email AS email,"
        + "       (CASE"
        + "            WHEN Active = 1 THEN CAST(1 AS BIT)"
        + "            ELSE CAST(0 AS BIT)"
        + "        END) AS active,"
        + "       RegTime AS created_at,"
        + "       UpgradeTime AS updated_at,"
        + "       Address AS address,"
        + "       Zalo AS zalo,"
        + "       FaceBook AS facebook,"
        + "   BannerImage as image,"
        + "  (CASE"
        + "            WHEN IsStore = 1 THEN CAST(1 AS BIT)"
        + "            ELSE CAST(0 AS BIT)"
        + "        END) AS is_store,"
        + "Birthday as birthday,"
        + "Password as password,"
        + "(CASE"
        + "            WHEN Gender = 1 THEN 'MALE'"
        + "            ELSE 'FEMALE'"
        + "        END) AS gender,"
        + "Logo as avatar,"
        + "AlterEmail as second_email "
        + "FROM dbo.Styles_Users";
  }

  //  Bảng dích bên DB mới
  @Override
  public String getDestTable() {
    return "users.user";
  }

  @Override
  public List<String> getEnumColumns() {
    return Arrays.asList("gender");
  }

  @Override
  public List<String> afterConvertQuery() {
    return Arrays.asList(
        "update users.user set avatar=concat('/images/2022-04-11/',split_part(avatar,'/',array_upper(string_to_array(avatar,'/'), 1))) where avatar is not null and avatar <> ''",
        "update users.user set image=concat('/images/2022-04-11/',split_part(image,'/',array_upper(string_to_array(image,'/'), 1))) where image is not null and image <> ''");
  }
}
