package com.tradeline.convert.utils.impl;

import com.tradeline.convert.utils.BaseUntil;

import java.util.Arrays;
import java.util.List;

public class Rating extends BaseUntil {
    @Override
    public String getSourceSelectQuery() {
        return "select ID as id, Rating as rating, ID_User as user_id, ID_Product as product_id, 'APPROVED' as state, AddTime as created_at from  \n" +
                "Styles_Shops_Product_Ratings";
    }

    @Override
    public String getDestTable() {
        return "products.rating";
    }

    @Override
    public List<String> getEnumColumns() {
        return Arrays.asList("state");
    }
}
