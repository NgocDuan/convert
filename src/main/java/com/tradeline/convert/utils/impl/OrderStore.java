package com.tradeline.convert.utils.impl;

import com.tradeline.convert.utils.BaseUntil;

import java.util.Arrays;
import java.util.List;

public class OrderStore extends BaseUntil {
  public static void main(String[] args) throws Exception {
    BaseUntil util = new OrderStore();
    util.execute();
  }

  @Override
  public String getSourceSelectQuery() {
    return "select so.ID_Order as id, uo.OrderCode as code, ID_Company as store_id,so.ID_User as user_id,\n" +
            "'{' + '\"name\":' + '\"'  + so.SOrderName + '\"' + ',' + '\"phone\":' + '\"'  + so.SOrderPhone + '\"' + ','  + '\"email\":' + '\"' +so.OrderEmail + '\"' + ','  + '\"avatar\": null' + '}' as store_info,\n" +
            "'{' + '\"name\":' + '\"'  + so.SOrderName + '\"' + ',' + '\"email\":' + '\"'  + so.OrderEmail + '\"' + ','  + '\"wardId\":1' + ',' + '\"address\":' + '\"' +so.BOrderAddress + '\"'+ ',' + '\"districtId\":1' + ','+ '\"provinceId\":1' + '}' as store_address,\n" +
            "so.ID_Order as order_id, uo.Total as total_money, 1 as shipping_fee, 1 as shipping_actual_fee, 'FINISH' as state,\n" +
            "'PAID' as payment_state, so.OrderTime as created_at\n" +
            "from  Styles_Shops_Orders so\n" +
            " inner join Styles_Users_Orders uo \n" +
            "on uo.ID_Order = so.ID_Order";
  }

  @Override
  public String getDestTable() {
    return "orders.order_store";
  }

  @Override
  public List<String> getEnumColumns() {
    return Arrays.asList("state", "payment_state");
  }

  @Override
  public List<String> getJsonColumns() {
    return Arrays.asList("store_info", "store_address");
  }
}
