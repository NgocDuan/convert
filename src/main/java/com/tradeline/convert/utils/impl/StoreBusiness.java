package com.tradeline.convert.utils.impl;

import com.tradeline.convert.utils.BaseUntil;

import java.util.Arrays;
import java.util.List;

public class StoreBusiness extends BaseUntil {
  @Override
  public String getSourceSelectQuery() {
    return "select sui.ID_User as id, su.CompanyName as company_name,su.UserCode as business_code,  RegTime as created_at, UpgradeTime as updated_at, sui.Image as company_avatar from Styles_Users_Images sui inner join Styles_Users su on sui.ID_User = su.ID_User";
  }

  @Override
  public String getDestTable() {
    return "stores.store_business";
  }


  @Override
  public List<String> afterConvertQuery() {
    return Arrays.asList(
        "update stores.store_business set company_avatar=concat('/images/2022-04-11/',split_part(image,'/',array_upper(string_to_array(image,'/'), 1))) where company_avatar is not null and company_avatar <> ''");
  }

  @Override
  public List<String> getEnumColumns() {
    return null;
  }
}
