package com.tradeline.convert.utils.impl;

import com.tradeline.convert.utils.BaseUntil;

import java.util.Arrays;
import java.util.List;

public class Category extends BaseUntil {
    @Override
    public String getSourceSelectQuery() {
        return "select sct.ID_Catalog as id, sct.CatalogName as name,sc.ID_Parent as parent_id, sc.Level as level, sc.Image as image,\n" +
                "case when sc.Active = 1 then 'ACTIVE' else 'INACTIVE' end as state, 1 as ref_id, 'PRODUCT' as type,  \n" +
                "case when sc.DisplayInMenu = 1 then cast(0 as bit) else cast(1 as bit) end as is_last, cast(1 as bit) as category_old\n" +
                "from Styles_Shops_Catalogs_Translation sct inner join Styles_Shops_Catalogs sc \n" +
                "on sct.ID_Catalog = sc.ID_Catalog where sct.LanguageCode = 'vi'";
    }

    @Override
    public String getDestTable() {
        return "products.category";
    }

    @Override
    public List<String> getEnumColumns() {
        return Arrays.asList("state","type");
    }
}
