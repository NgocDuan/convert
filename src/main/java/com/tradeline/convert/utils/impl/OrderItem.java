package com.tradeline.convert.utils.impl;

import com.tradeline.convert.utils.BaseUntil;

import java.util.Arrays;
import java.util.List;

public class OrderItem extends BaseUntil {
  public static void main(String[] args) throws Exception {
    BaseUntil util = new OrderItem();
    util.execute();
  }

  @Override
  public String getSourceSelectQuery() {
    return "select ID as id, 'FINISH' as state, ID_Product as product_id, so.ID_Order as order_store_id, so.ID_User as user_id,\n" +
            "Price as price, Amount as quantity, ID_Product as product_variant_id, '{\n" +
            "  \"name\": null,\n" +
            "  \"size\": null,\n" +
            "  \"image\": null,\n" +
            "  \"weight\": null\n" +
            "}' as product, '[\n" +
            "  {\n" +
            "    \"key\": null,\n" +
            "    \"value\": null\n" +
            "  },\n" +
            "  {\n" +
            "    \"key\": null,\n" +
            "    \"value\": null\n" +
            "  }\n" +
            "]' as attributes\n" +
            "from Styles_Shops_Order_Details sod inner join Styles_Shops_Orders so\n" +
            "on sod.ID_Order = so.ID_Order inner join Styles_Users_Orders uo \n" +
            "on uo.ID_Order = so.ID_Order";
  }

  @Override
  public String getDestTable() {
    return "orders.order_item";
  }

  @Override
  public List<String> getEnumColumns() {
    return Arrays.asList("state");
  }

  @Override
  public List<String> getJsonColumns() {
    return Arrays.asList("product", "attributes");
  }
}
