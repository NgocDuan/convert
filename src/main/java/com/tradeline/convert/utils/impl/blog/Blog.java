package com.tradeline.convert.utils.impl.blog;

import com.tradeline.convert.utils.BaseUntil;

import java.util.Arrays;
import java.util.List;

public class Blog extends BaseUntil {
  @Override
  public String getSourceSelectQuery() {
    return "select n.ID_News as id, t.Title as title, t.SummaryContent as excerpt,n.ID_Cate as category_id, 1 as author_id, t.Link_SEO as slug, 'ACTIVE' as state, n.HitsTotal as 'view', t.Content as content, n.[Image] as avatar from Styles_News n left join Styles_News_Translation t on n.ID_News = t.ID_News and t.LanguageCode = 'vi' where n.ID_Cate=23";
  }

  @Override
  public String getDestTable() {
    return "blogs.blog";
  }

  @Override
  public List<String> getEnumColumns() {
    return Arrays.asList("state");
  }

  @Override
  public List<String> afterConvertQuery() {
    return Arrays.asList(
        "update blogs.blog set avatar=concat('/images/2021-11-13/',split_part(avatar,'/',array_upper(string_to_array(avatar,'/'), 1))) where avatar is not null and avatar <> ''",
        "UPDATE blogs.blog SET content=REPLACE(content, '/UploadImages', 'http://api-dev.tradeline.vn/storage/images/2021-11-13')");
  }
}
