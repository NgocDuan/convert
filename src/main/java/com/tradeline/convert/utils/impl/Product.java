package com.tradeline.convert.utils.impl;

import com.tradeline.convert.utils.BaseUntil;

import java.util.Arrays;
import java.util.List;

public class Product extends BaseUntil {

  @Override
  public String getSourceSelectQuery() {
    return "select spt.ID_Product as id,sp.BrandName as brand_name, ID_Catalog as category_id, spt.ProductName as name, sp.Weight as weight, sp.ProductCode as sku,\n" +
            "spt.Link_SEO as name_slug,case when sp.ID_Currency = 0 then 2 else sp.ID_Currency end as currency_id,\n" +
            "case when sp.ID_Unit in (0,1026,1027,1030) then 1024 else sp.ID_Unit end as unit_id,\n" +
            "sp.AddTime as created_at, sp.EditTime as updated_at, sp.SupplyAbility as ability_to_provide,\n" +
            "case when spt.ProcessingTime = '01 ngày' then 1 else 2 end as order_processing_time, case when sp.ProductStatus = 'N' then 'PUBLIC' else 'DELETED' end as state, sp.MinOfQuantity as min_quantity_order, case when sp.BrandName is not null then 'NORMAL' end as type,\n" +
            "CAST(sp.UserName as bigint) as store_id, spt.Description as description, CAST(1 as bit)as data_old\n" +
            "from dbo.Styles_Shops_Products sp inner join dbo.Styles_Shops_Products_Translation spt\n" +
            "on sp.ID_Product = spt.ID_Product where spt.LanguageCode = 'vi' and sp.IsInTrash = 0 ";

  }

  @Override
  public String getDestTable() {
    return "products.product";
  }

  @Override
  public List<String> getEnumColumns() {
    return Arrays.asList("state", "type");
  }
}
