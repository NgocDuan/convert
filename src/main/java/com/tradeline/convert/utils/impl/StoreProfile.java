package com.tradeline.convert.utils.impl;

import com.tradeline.convert.utils.BaseUntil;

import java.util.Arrays;
import java.util.List;

public class StoreProfile extends BaseUntil {
    @Override
    public String getSourceSelectQuery() {
        return "select sut.ID_User as ID, '{'+ '\"' + MainProduct + '\"}' as main_product, RegTime AS created_at, UpgradeTime AS updated_at,\n" +
                " CompanyRepresentative as legal_representative, YearEstablished as founded_year, NumberOfMember as total_member, NumberOfEmployee as total_employee,\n" +
                " '{'+ '\"' + ExportMarkets + '\"}' as main_market, CAST(BusinessCapital as nvarchar) as registered_capital, TaxNumber as tax_code, TotalView as count_view\n" +
                "from Styles_Users su inner join Styles_Users_Translation sut on su.ID_User = sut.ID_User  \n" +
                "where su.IsStore = 1 and sut.LanguageCode = 'vi'  ";
    }

    @Override
    public String getDestTable() {
        return "stores.store_profile";
    }

    @Override
    public List<String> getEnumColumns() {
        return null;
    }

    @Override
    public List<String> getJsonColumns() {
        return Arrays.asList("main_product", "main_market");
    }
}
