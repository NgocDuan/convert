package com.tradeline.convert.utils.impl;

import com.tradeline.convert.utils.BaseUntil;

import java.util.Arrays;
import java.util.List;

public class Order extends BaseUntil {
  public static void main(String[] args) throws Exception {
    BaseUntil util = new Order();
    util.execute();
  }

  @Override
  public String getSourceSelectQuery() {
    return "select suo.ID_Order as id, suo.ID_User as user_id, Total as total_money, Discount as discount_money, 1 as shipping_money, Total as total_payment, \n"
        + "OrderCode as code, '{' + '\"name\":' + '\"'  + OrderName + '\"' + ',' + '\"phone\":' + '\"'  + OrderPhone + '\"' + ','  + '\"wardId\":1' + ',' + '\"address\":' + '\"' +OrderAddress + '\"'+ ',' + '\"districtId\":1' + ','+ '\"provinceId\":1' + '}' as delivery_address, case when ID_Payment = 1 then 'PAY' else 'COD' end as payment_type, 'PAID' as payment_state,\n"
        + "'{' + '\"name\":' + '\"'  + OrderName + '\"' + ',' + '\"phone\":' + '\"'  + OrderPhone + '\"' + ','  + '\"email\":' + '\"' +OrderEmail + '\"' + '}' as user_info ,AddTime as created_at, SendTime as updated_at\n"
        + "from Styles_Users_Orders suo inner join Styles_Users_Orders_Invoices_Bills suoi on suo.ID_Order = suoi.ID_Order";
  }

  @Override
  public String getDestTable() {
    return "orders.orders";
  }

  @Override
  public List<String> getEnumColumns() {
    return Arrays.asList("payment_type", "payment_state");
  }

  @Override
  public List<String> getJsonColumns() {
    return Arrays.asList("delivery_address", "user_info");
  }
}
