package com.tradeline.convert.utils.impl;

import com.tradeline.convert.utils.BaseUntil;

import java.util.List;

public class Inventory extends BaseUntil {

    @Override
    public String getSourceSelectQuery() {
        return "select spt.ID_Product as product_variant_id, 1000 as quantity, 1000 as sold_quantity,  spt.ID_Product as product_id, sp.AddTime as created_at, sp.EditTime as updated_at \n" +
                "from dbo.Styles_Shops_Products sp inner join dbo.Styles_Shops_Products_Translation spt\n" +
                "on sp.ID_Product = spt.ID_Product where spt.LanguageCode = 'vi'";
    }

    @Override
    public String getDestTable() {
        return "products.inventory";
    }

    @Override
    public List<String> getEnumColumns() {
        return null;
    }
}
