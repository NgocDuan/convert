package com.tradeline.convert.utils.impl;

import com.tradeline.convert.utils.BaseUntil;

import java.util.List;

public class Unit extends BaseUntil {
    @Override
    public String getSourceSelectQuery() {
        return "select su.ID_Unit as id, UnitName as name\n" +
                "from dbo.Styles_Shops_Units_Translation sut inner join dbo.Styles_Shops_Units su on sut.ID_Unit = su.ID_Unit\n" +
                " where LanguageCode = 'vi'";
    }

    @Override
    public String getDestTable() {
        return "public.unit";
    }

    @Override
    public List<String> getEnumColumns() {
        return null;
    }
}
