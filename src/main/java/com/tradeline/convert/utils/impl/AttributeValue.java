package com.tradeline.convert.utils.impl;

import com.tradeline.convert.utils.BaseUntil;

import java.util.Arrays;
import java.util.List;

public class AttributeValue extends BaseUntil {

    @Override
    public String getSourceSelectQuery() {
        return "select spfvt.ID_Variant as id, spfv.ID_Feature as attribute_id, 'PUBLIC' as state, spfvt.VariantName as value\n" +
                "from dbo.Styles_Shops_Products_Feature_Variants spfv inner join dbo.Styles_Shops_Products_Feature_Variants_Translation spfvt\n" +
                "on spfv.ID_Variant = spfvt.ID_Variant where spfvt.LanguageCode = 'vi' ";

    }

    @Override
    public String getDestTable() {
        return "products.attribute_value";
    }

    @Override
    public List<String> getEnumColumns() {
        return Arrays.asList("state");
    }
}
