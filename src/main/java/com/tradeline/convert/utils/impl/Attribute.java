package com.tradeline.convert.utils.impl;

import com.tradeline.convert.utils.BaseUntil;

import java.util.Arrays;
import java.util.List;

public class Attribute extends BaseUntil {

    @Override
    public String getSourceSelectQuery() {
        return "select pf.ID_Feature as id, pft.FeatureName as name, 'PUBLIC' state,CAST(1 as BIT) as required from \n" +
                "Styles_Shops_Products_Features pf inner join Styles_Shops_Products_Features_Translation pft\n" +
                "on pf.ID_Feature = pft.ID_Feature where pft.LanguageCode = 'vi'";

    }

    @Override
    public String getDestTable() {
        return "products.attribute";
    }

    @Override
    public List<String> getEnumColumns() {
        return Arrays.asList("state");
    }
}
