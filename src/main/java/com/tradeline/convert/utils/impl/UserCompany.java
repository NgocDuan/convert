package com.tradeline.convert.utils.impl;

import com.tradeline.convert.utils.BaseUntil;

import java.util.Arrays;
import java.util.List;

public class UserCompany extends BaseUntil {
  @Override
  public String getSourceSelectQuery() {
    return "select sut.ID_User as id, su.Address as active_address, su.RegisteredAddress as registered_address,su.RegTime AS created_at, su.UpgradeTime AS updated_at,\n"
        + " su.CompanyName as name, su.Website as website\n"
        + "from Styles_Users su inner join Styles_Users_Translation sut on su.ID_User = sut.ID_User where su.IsStore =0 and sut.LanguageCode = 'vi' and su.IsCompany = 1";
  }

  @Override
  public String getDestTable() {
    return "users.user_company";
  }

  @Override
  public List<String> getEnumColumns() {
    return null;
  }

  @Override
  public List<String> getJsonColumns() {
    return Arrays.asList("type", "main_product");
  }
}
