package com.tradeline.convert.utils.impl.posts;

import com.tradeline.convert.utils.BaseUntil;

import java.util.List;

public class PostBusinessProduct extends BaseUntil {
    @Override
    public String getSourceSelectQuery() {
        return "select suat.ID_Agency as id_post, category.CatalogName as category ,suat.Description as description,suat.Title as title, ProductName as product, suat.NumberOfEmployee as person ,\n" +
                "1 as province_id, UnitName as unit, suat.Phone as phone, sua.ID_Catalog as category_id\n" +
                "from Styles_Users_Agencys sua inner join Styles_Users_Agencys_Translation suat on sua.ID_Agency = suat.ID_Agency\n" +
                "inner join (select ID_Catalog, CatalogName from Styles_Shops_Catalogs_Translation ct where LanguageCode = 'vi') as category on sua.ID_Catalog = category.ID_Catalog\n" +
                "inner join (select ID_Unit, UnitName from Styles_Shops_Units_Translation where LanguageCode = 'vi') as unit on sua.ID_Unit = unit.ID_Unit where sua.IsAgency = 1";
    }

    @Override
    public String getDestTable() {
        return "posts.post_business_product";
    }

    @Override
    public List<String> getEnumColumns() {
        return null;
    }
}
