package com.tradeline.convert.utils.impl.blog;

import com.tradeline.convert.utils.BaseUntil;

import java.util.Arrays;
import java.util.List;

public class BlogCate extends BaseUntil {
  @Override
  public String getSourceSelectQuery() {
    return "select c.ID_Cate as id ,CAST(1 AS BIT) as active, t.CategoryName as name,t.Link_SEO as slug, 'PUBLIC' as state from Styles_News_Category c left join Styles_News_Category_Translation t on c.ID_Cate = t.ID_Cate and t.LanguageCode = 'vi'";
  }

  @Override
  public String getDestTable() {
    return "blogs.category";
  }

  @Override
  public List<String> getEnumColumns() {
    return Arrays.asList("state");
  }
}
