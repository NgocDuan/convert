package com.tradeline.convert.utils;

import com.tradeline.convert.common.DbConnect;
import com.tradeline.convert.common.Utilities;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public abstract class BaseUntil {
  protected Connection postgreConnection = null;
  protected Connection sqlServeConnection = null;

  public void doInitialize() throws Exception {
    connect();
  }

  private void connect() throws SQLException {
    postgreConnection = DbConnect.getConnectionPostgre();
    sqlServeConnection = DbConnect.getConnectionSqlserver();
  }

  public abstract String getSourceSelectQuery();

  public abstract String getDestTable();

  public List<String> afterConvertQuery() {
    return null;
  }

  public abstract List<String> getEnumColumns();

  public List<String> getJsonColumns() {
    return new ArrayList<>();
  }

  public void execute() throws Exception {
    try {
      doInitialize();
      postgreConnection.setAutoCommit(false);
      Utilities.executeUpdate(postgreConnection, "delete from " + getDestTable() + ";");
      Utilities.executeUpdate(postgreConnection, "TRUNCATE " + getDestTable() + " CASCADE;");
      System.out.println(String.format("clear table %s", getDestTable()));
      int[] res =
          Utilities.executeSelectInsert(
              sqlServeConnection,
              postgreConnection,
              getSourceSelectQuery(),
              getDestTable(),
              getEnumColumns(),
              getJsonColumns());
      System.out.println(String.format("convert %s to table %s", res[0], getDestTable()));
      System.out.println(String.format("convert %s row success", getDestTable()));
      if (afterConvertQuery() != null) {
        for (String query : afterConvertQuery()) {
          Utilities.executeUpdate(postgreConnection, query);
        }
        System.out.println(String.format("Run after convert"));
      }
      postgreConnection.commit();
    } catch (Exception e) {
      e.printStackTrace();
      System.out.println(String.format("convert %s error", getDestTable()));
      postgreConnection.rollback();
    } finally {
      sqlServeConnection.close();
      postgreConnection.close();
    }
    System.out.println("=");
  }
}
