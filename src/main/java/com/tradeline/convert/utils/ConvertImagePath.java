package com.tradeline.convert.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class ConvertImagePath {
  public static void main(String[] args) throws IOException {
    File file = new File("D:\\Blog");
    listFilesForFolder(file);
  }

  public static void listFilesForFolder(final File folder) throws IOException {
    for (final File fileEntry : folder.listFiles()) {
      if (fileEntry.isDirectory()) {
        listFilesForFolder(fileEntry);
      } else {
        System.out.println(fileEntry.getAbsolutePath());
        Path originalPath = Paths.get("D:\\convertimage" + "\\" + fileEntry.getName());
        Files.copy(
            Paths.get(fileEntry.getAbsolutePath()),
            originalPath,
            StandardCopyOption.REPLACE_EXISTING);
      }
    }
  }
}
