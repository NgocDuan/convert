package com.tradeline.convert.utils.types;

public enum EnumValue {
  WAIT,
  PAID,
  CANCLED,
  COD,
  PAY,
  APPROVED,
  REJECT,
  FEMALE,
  MALE,
  PENDING,
  PUBLIC,
  DELETED,
  ACTIVE,
  INACTIVE,
  PRODUCT,
  MARKETPLACE,
  DRAFT,
  REPORTED,
  SERVICE,
  CLASSIFY,
  NORMAL,
  WHOLESALE,
  REFERENCE,
  BLOCK
}
