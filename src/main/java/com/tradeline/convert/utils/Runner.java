package com.tradeline.convert.utils;

import com.tradeline.convert.utils.impl.*;
import com.tradeline.convert.utils.impl.blog.Blog;
import com.tradeline.convert.utils.impl.blog.BlogCate;
import com.tradeline.convert.utils.impl.posts.Post;
import com.tradeline.convert.utils.impl.posts.PostBidInvitation;
import com.tradeline.convert.utils.impl.posts.PostBusinessProduct;
import com.tradeline.convert.utils.impl.posts.PostRegisterTeam;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.lang.reflect.Constructor;

@Component
public class Runner implements CommandLineRunner {
    @Override
    public void run(String... args) throws Exception {
        Class<?>[] ALL_CLASSES = new Class<?>[]{
 //               User.class,
//                UserAddress.class,
//                UserCompany.class,
//                UsersRole.class,
//                Category.class,
//                Unit.class,
////                Currency.class,
//                Store.class,
//                StorePackage.class,
//                StoreContact.class,
//              StoreProfile.class,
//               Product.class,
//              ProductVariant.class,
//                Inventory.class,
//               ProductImage.class,
//
//               Attribute.class,
//               AttributeValue.class,
//                RelProductAttribute.class,
//                BlogCate.class,
//                Blog.class
//                Post.class,
//                PostBusinessProduct.class,
//                PostBidInvitation.class,
//                PostRegisterTeam.class
        };

        for (Class<?> clazz : ALL_CLASSES) {
            Constructor constructor = clazz.getConstructor();
            BaseUntil until = (BaseUntil) constructor.newInstance();
            until.execute();
        }
        System.out.println("==================Convert done=============");
    }
}
