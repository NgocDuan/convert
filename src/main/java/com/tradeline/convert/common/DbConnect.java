package com.tradeline.convert.common;

import com.tradeline.convert.config.AppContext;
import com.tradeline.convert.config.EnvironmentVariable;
import org.apache.commons.dbcp.BasicDataSourceFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class DbConnect {
    private static final DbConnect INSTANCE = new DbConnect();
    private final Map<String, DataSource> mDataSources = new HashMap<String, DataSource>();
    private final EnvironmentVariable env;

    private DbConnect() {
        // nothing
        this.env = AppContext.getBean(EnvironmentVariable.class);
    }

    public static Connection getConnection(String name) throws SQLException {
        DataSource dataSource = INSTANCE.mDataSources.get(name);
        if (dataSource != null) {
            return dataSource.getConnection();
        }
        try {
            Properties properties = INSTANCE.env.mapDbsProperties().get(name);
            dataSource = BasicDataSourceFactory.createDataSource(properties);
            INSTANCE.mDataSources.put(name, dataSource);
            return dataSource.getConnection();
        } catch (Exception e) {
            throw new RuntimeException("Could not create connection.", e);
        }
    }

    public static Connection getConnectionPostgre() throws SQLException {
        return getConnection(EnvironmentVariable.DB_POSTGRESQL);
    }

    public static Connection getConnectionSqlserver() throws SQLException {
        return getConnection(EnvironmentVariable.DB_SQLSERVER);
    }
}
