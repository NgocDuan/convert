/** $Id$ */
package com.tradeline.convert.common;

import com.tradeline.convert.utils.types.EnumValue;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang.StringUtils;

import java.sql.*;
import java.util.List;
import java.util.Locale;
import java.util.function.Function;

/**
 * Mssql utilities
 *
 * @author miijima6
 */
public class Utilities {

  public static <T> T executeQuery(
      Connection conn, String query, Function<ResultSet, T> createReturnValue, Object... parameters)
      throws SQLException {
    PreparedStatement ps = null;
    ResultSet rs = null;
    try {
      ps = conn.prepareStatement(query);
      for (int i = 0; i < parameters.length; i++) {
        ps.setObject(i + 1, parameters[i]);
      }
      rs = ps.executeQuery();
      return createReturnValue.apply(rs);
    } finally {
      DbUtils.closeQuietly(ps);
      DbUtils.closeQuietly(rs);
    }
  }

  public static int executeUpdate(Connection conn, String updateQuery, Object... parameters)
      throws SQLException {
    PreparedStatement st = null;
    try {
      st = conn.prepareStatement(updateQuery);
      for (int i = 0; i < parameters.length; i++) {
        st.setObject(i + 1, parameters[i]);
      }
      return st.executeUpdate();
    } finally {
      DbUtils.closeQuietly(st);
    }
  }

  /*
   * Create database query
   */
  public static String createQueryInsert(String tableName, String... columnNames) {
    StringBuilder buffer = new StringBuilder();
    buffer.append("INSERT INTO ").append(tableName).append(" (");
    int columnCount = columnNames.length;
    for (int i = 0; i < columnCount; i++) {
      buffer.append(columnNames[i].toLowerCase(Locale.ROOT));
      if (columnCount != (i + 1)) {
        buffer.append(",");
      }
    }
    buffer.append(") VALUES (");
    for (int i = 0; i < columnCount; i++) {
      buffer.append("?");
      if (columnCount != (i + 1)) {
        buffer.append(",");
      }
    }
    buffer.append(")");
    return buffer.toString();
  }

  public static boolean tableHasIdentity(Connection conn, String tableName) throws SQLException {
    return Utilities.fetchRowInt(conn, "SELECT count(*) from " + tableName + ";", 0) > 0;
  }

  public static int[] executeSelectInsert(
      Connection srcConn,
      Connection destConn,
      String selectQuery,
      String destTableName,
      List<String> enumColumns,
      List<String> jsonColumns)
      throws SQLException {
    PreparedStatement st1 = null;
    PreparedStatement st2 = null;
    ResultSet rs1 = null;
    try {
      // select
      st1 = srcConn.prepareStatement(selectQuery);
      st1.setFetchSize(1000);
      rs1 = st1.executeQuery();
      ResultSetMetaData metaData1 = rs1.getMetaData();
      int columnCount1 = metaData1.getColumnCount();
      String[] columnNames = new String[columnCount1];
      for (int i = 0; i < columnCount1; i++) {
        columnNames[i] = metaData1.getColumnLabel(i + 1);
      }

      // build merge sql
      String mergeSql = createQueryInsert(destTableName, columnNames);
      // merge
      st2 = destConn.prepareStatement(mergeSql);

      var tableHasIdentity = Utilities.tableHasIdentity(destConn, destTableName);
      if (tableHasIdentity) {
        //        st2.executeUpdate("SET IDENTITY_INSERT " + destTableName + " ON;");
      }

      boolean isSQLite =
          StringUtils.equals(srcConn.getMetaData().getDriverName(), "SQLiteJDBC")
              || StringUtils.equals(destConn.getMetaData().getDriverName(), "SQLiteJDBC");
      int queryCount = 0;
      int affectedCount = 0;
      while (rs1.next()) {
        for (int i = 1; i <= columnCount1; i++) {
          int columnType1 = metaData1.getColumnType(i);
          switch (columnType1) {
            case Types.CHAR:
            case Types.VARCHAR:
            case Types.LONGVARCHAR:
            case Types.LONGNVARCHAR:
            case Types.NVARCHAR:
              if (enumColumns != null && enumColumns.contains(metaData1.getColumnName(i))) {
                st2.setObject(i, EnumValue.valueOf(rs1.getString(i)), Types.OTHER);
              } else if (jsonColumns != null && jsonColumns.contains(metaData1.getColumnName(i))) {
                st2.setObject(i, rs1.getString(i), Types.OTHER);
              } else {
                st2.setString(i, rs1.getString(i));
              }
              break;
            case Types.NUMERIC:
            case Types.DECIMAL:
            case Types.DOUBLE:
            case Types.FLOAT:
              if (isSQLite) {
                st2.setObject(i, rs1.getObject(i));
              } else {
                st2.setBigDecimal(i, rs1.getBigDecimal(i)); // SQLite is not implemented
              }
              break;
            case Types.BIT:
              st2.setBoolean(i, rs1.getBoolean(i));
              break;
            case Types.SMALLINT:
            case Types.TINYINT:
            case Types.INTEGER:
              st2.setInt(i, rs1.getInt(i));
              break;
            case Types.BIGINT:
              st2.setLong(i, rs1.getLong(i));
              break;
            case Types.DATE:
            case Types.TIMESTAMP:
              st2.setTimestamp(i, rs1.getTimestamp(i));
              break;
            case Types.NULL:
              st2.setObject(i, rs1.getObject(i));
              break;
            default:
              throw new RuntimeException(
                  String.format(
                      "Unsupport column type: %s(%d), name: %s",
                      metaData1.getColumnTypeName(i),
                      metaData1.getColumnType(i),
                      metaData1.getColumnName(i)));
          }
        }
        st2.addBatch();
        queryCount++;
        if ((queryCount % 10000) == 0) {
          affectedCount += sum(st2.executeBatch());
        }
      }

      affectedCount += sum(st2.executeBatch());
      if (tableHasIdentity) {
        //        st2.executeUpdate("SET IDENTITY_INSERT " + destTableName + " OFF;");
      }
      return new int[] {queryCount, affectedCount};
    } finally {
      DbUtils.closeQuietly(st2);
      DbUtils.closeQuietly(rs1);
      DbUtils.closeQuietly(st1);
    }
  }

  public static int fetchRowInt(
      Connection conn, String query, int defaultValue, Object... parameters) throws SQLException {
    PreparedStatement st = null;
    ResultSet rs;
    try {
      st = conn.prepareStatement(query);
      for (int i = 0; i < parameters.length; i++) {
        st.setObject(i + 1, parameters[i]);
      }
      rs = st.executeQuery();
      if (rs.next()) {
        String result = rs.getString(1);
        if (result == null) {
          return defaultValue;
        }
        return Integer.parseInt(result);
      }
      return defaultValue;
    } finally {
      DbUtils.closeQuietly(st);
    }
  }

  public static int sum(int[] items) {
    int result = 0;
    for (int item : items) {
      result += item;
    }
    return result;
  }
}
