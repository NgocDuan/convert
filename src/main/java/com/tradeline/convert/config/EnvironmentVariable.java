package com.tradeline.convert.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@Configuration
public class EnvironmentVariable {
    public static final String DB_POSTGRESQL = "db_postgresql";
    public static final String DB_SQLSERVER = "db_sqlserver";

    @Bean
    @ConfigurationProperties(prefix = "db.postgresql")
    public Properties pgProperties() {
        return new Properties();
    }

    @Bean
    @ConfigurationProperties(prefix = "db.sqlserver")
    public Properties sqlServerProperties() {
        return new Properties();
    }

    @Bean
    public Map<String, Properties> mapDbsProperties() {
        Map<String, Properties> map = new HashMap<String, Properties>();
        map.put(DB_POSTGRESQL, pgProperties());
        map.put(DB_SQLSERVER, sqlServerProperties());
        return map;
    }
}
