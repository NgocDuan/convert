package com.tradeline.convert;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TradelineConvertApplication {

    public static void main(String[] args) {
        SpringApplication.run(TradelineConvertApplication.class, args);
    }

}
